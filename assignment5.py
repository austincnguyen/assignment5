#!/usr/bin/env python3

# ------------------------------------------------------------------
# assignments/assignment5.py
# Fares Fraij
# ------------------------------------------------------------------

#-----------
# imports
#-----------

from flask import Flask, render_template, request, redirect, url_for, jsonify, Response
import random
import json

app = Flask(__name__)

# Dictionary of books
books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

@app.route('/book/JSON/')
def bookJSON():
    # <your code>
    json_version = json.dumps(books)
    return Response(json_version)

@app.route('/')
@app.route('/book/')
def showBook():
    # <your code>
    return render_template('showBook.html', books = books)
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    # <your code>
    cur = 1
    sort_list = sorted(books, key=lambda x: x['id'])
    for i in sort_list:
        cur_id = int(i["id"])
        if cur == cur_id:
            cur += 1
    if request.method == 'POST':
        search_key = request.form.get('name')
        new_book = {'title': search_key, 'id': str(cur)}
        books.append(new_book)
        return render_template('showBook.html', books = books)
    else:
	    return render_template('newBook.html')

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    # <your code>
    for i in books:
        if i['id'] == str(book_id):
            book_title = i['title']
            break

    if request.method == 'POST':
        search_key = request.form.get('name')
        for i in books:
            if i['id'] == str(book_id):
                i['title'] = search_key
                break
        return render_template('showBook.html', books = books)
    else:
	    return render_template('editBook.html', book_id = book_id, book_title=book_title)
	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    # <your code>
    for i in books:
        if i['id'] == str(book_id):
            book_title = i['title']
            break

    if request.method == 'POST':
        for i in books:
            if i['id'] == str(book_id):
                books.remove(i)
                break
        return render_template('showBook.html', books = books)
    else:
	    return render_template('deleteBook.html', book_id = book_id, book_title=book_title)

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)